<?php

namespace Drupal\commerce_import\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_price\Price;

/**
 * InitMigrations.
 */
class UpdateProducts {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The product storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $productStorage;

  /**
   * The variation storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $variationStorage;

  /**
   * Creates a new Pileline manager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Manager service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->productStorage = $entity_type_manager->getStorage('commerce_product');
    $this->variationStorage = $entity_type_manager->getStorage('commerce_product_variation');
    $this->map = [
      'vid',
      'sku',
      'price',
      'price2',
      'status',
      'pid',
      'status',
      'title',
      'link',
    ];
  }

  /**
   * Init.
   */
  public function run($path) {
    \Drupal::messenger()->addWarning($path);
    $i = 0;
    $rows = [];
    $handle = fopen($path, 'r');
    while ($line = fgetcsv($handle, 4096)) {
      if (!$line) {
        continue;
      }
      $row = [];
      foreach ($this->map as $key => $name) {
        $row[$name] = $this->clearCell($line, $key);
      }
      $rows[$i++] = $row;
    }
    fclose($handle);
    array_shift($rows);
    foreach ($rows as $key => $value) {
      $variation = $this->variationStorage->load($value['vid']);
      if (is_object($variation)) {
        // $currency = $variation->getPrice()->getCurrencyCode();
        $price = new Price($value['price'], 'USD');
        $price2 = new Price($value['price2'], 'USD');
        $variation->set('price', $price);
        $variation->set('list_price', $price2);
        $variation->save();
      }
      else {
        \Drupal::messenger()->addWarning($value['vid']);
      }
    }
    return $rows;
  }

  /**
   * Clear CSV Cell.
   */
  private function clearCell($line, $k) {
    return isset($line[$k]) ? trim($line[$k]) : NULL;
  }

  /**
   * Run Batch.
   */
  private function batch() {
    $batch = [
      'operations' => [],
      'title' => $this->t('Updating product variation prices...'),
    ];
    if ($sku && $price && is_numeric($price)) {
      $batch['operations'][] = [[$this, 'updateProductPrice'], [$sku, $price]];
      $counter++;
    }
  }

  /**
   * Init.
   */
  public function export() {
    $rows = [];
    $products = $this->query($this->productStorage);
    $host = \Drupal::request()->getSchemeAndHttpHost();
    foreach ($this->query($this->variationStorage) as $id => $entity) {
      $pid = $entity->product_id->target_id;
      $product = $products[$pid];
      $rows[$id] = [
        'id' => $id,
        'sku' => $entity->sku->value,
        'price' => floatval($entity->price[0]->number),
        'price2' => floatval($entity->list_price[0]->number),
        'status' => $entity->status->value,
        'pflag' => 0,
        'pid' => $pid,
        'status' => $product->status->value,
        'title' => $product->title->value,
        'link' => "$host/product/{$id}",
      ];
    }
    return $rows;
  }

  /**
   * Import.
   */
  private function import() {
    $config = $this->configFactory->get('commerce_import.settings');
    return 'failure';
  }

  /**
   * Query.
   */
  public function query($storage) {
    $entities = [];
    $query = $storage->getQuery()
      ->accessCheck(TRUE)
      ->sort('created', 'ASC');
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

}
