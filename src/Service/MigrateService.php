<?php

namespace Drupal\commerce_import\Service;

/**
 * Get MigrateService.
 */
class MigrateService implements MigrateServiceInterface {

  /**
   * Get migrations.
   */
  public function getCommerceGroup()
  {
    $migrations = []; // Initialize as an empty array
    $manager = NULL; // Initialize as NULL
    try {
      $manager = \Drupal::service('plugin.manager.migration');
    } catch (\Exception $e) {
      return FALSE;
    }
    $date_formatter = \Drupal::service('date.formatter');
    if ($manager) {
      $plugins = $manager->createInstances([]);
      if (!empty($plugins)) {
        foreach ($plugins as $id => $migration) {
          $migrations['status'] = TRUE;
          if ($migration->migration_group == 'commerce') {
            $source_plugin = $migration->getSourcePlugin();
            $map = $migration->getIdMap();
            if ($migration->getStatusLabel() !== 'Idle') {
              $migrations['status'] = FALSE;
            }
            $last = 'N/A';
            $time = \Drupal::keyValue('migrate_last_imported')->get($id, FALSE);
            if (is_numeric($time)) {
              $last = $date_formatter->format($time / 1000, 'custom', 'dM H:i:s');
            }
            $migrations['list'][$id] = [
              'label' => $migration->label(),
              'status' => $migration->getStatusLabel(),
              'total' => $source_plugin->count(),
              'imported' => (int) $map->importedCount(),
              'unprocessed' => $source_plugin->count() - (int) $map->importedCount(),
              'messages' => $map->messageCount(),
              'last' => $last,
            ];
          }
        }
      }
    }
    return $migrations;
  }

}
