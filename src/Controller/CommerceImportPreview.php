<?php

namespace Drupal\commerce_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for status page.
 */
class CommerceImportPreview extends ControllerBase {

  /**
   * Catalog.
   */
  public function catalog() {
    $tree = [];
    $plugin = $this->initPlugin();
    $catalog = $plugin->catalog();
    foreach ($catalog as $key => $item) {
      $key = $item['id'];
      if (!$item['parent']) {
        $tree[$key] = $item;
      }
      else {
        $parent = $item['parent'];
        if (isset($tree[$parent])) {
          $tree[$parent]['children'][$key] = $item;
        }
        else {
          $p = $catalog[$parent]['parent'];
          $tree[$p]['children'][$parent]['children'][$key] = $item;
        }
      }
    }
    $html = $this->catalogTree($tree, TRUE);
    return [
      'catalog' => [
        '#markup' => "<div id='jstree'>{$html}</div>",
        '#attached' => ['library' => ['commerce_import/jstree']],
      ],
    ];
  }

  /**
   * Render.
   */
  public function catalogTree($groups, $parent = FALSE) {
    $output = '<ul>';
    if (!empty($groups)) {
      foreach ($groups as $group) {
        $data = '';
        if ($parent) {
          $data = "data-jstree='{ \"opened\" : true }' ";
        }
        $output .= "<li $data>";
        $output .= $group['name'];
        if (!empty($group['children'])) {
          $output .= $this->catalogTree($group['children']);
        }
        $output .= '</li>';
      }
      $output .= '</ul>';
    }
    return $output;
  }

  /**
   * Products.
   */
  public function products() {
    $plugin = $this->initPlugin();
    $products = $plugin->product();
    dsm($products);
    return [
      'output' => ['#markup' => 'products'],
    ];
  }

  /**
   * Variations.
   */
  public function variations() {
    $plugin = $this->initPlugin();
    $variations = $plugin->variation();
    dsm($variations);
    return [
      'output' => ['#markup' => 'characteristics + variations'],
    ];
  }

  /**
   * Variations.
   */
  public function images() {
    $plugin = $this->initPlugin();
    $images = $plugin->image();
    dsm($images);
    return [
      'output' => ['#markup' => 'image'],
    ];
  }

  /**
   * Terms.
   */
  public function terms() {
    $plugin = $this->initPlugin();
    $terms = $plugin->term();
    dsm($terms);
    return [
      'output' => ['#markup' => 'terms'],
    ];
  }

  /**
   * Init.
   */
  private function initPlugin($cfg = []) {
    $type = \Drupal::service('plugin.manager.commerce_import');
    $this->definitions = $type->getDefinitions();
    $pid = \Drupal::config('commerce_import.settings')->get('source_plugin');
    $this->plugin = $type->createInstance($pid, $cfg);
    return $this->plugin;
  }

  /**
   * Header.
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Migration'),
      'status' => $this->t('Status'),
      'total' => $this->t('Total'),
      'imported' => $this->t('Imported'),
      'unprocessed' => $this->t('Unprocessed'),
      'messages' => $this->t('Messages'),
      'last' => $this->t('Last'),
    ];
    return $header;
  }

}
