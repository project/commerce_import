<?php

namespace Drupal\commerce_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for status page.
 */
class CommerceImportStatus extends ControllerBase {

  /**
   * Page.
   */
  public function page() {
    $rows = [];
    $migrations = \Drupal::service('commerce_import.migrate')->getCommerceGroup();
    $output = "";
    if ($migrations) {
      $output .= "<h3>State</h3>";
      if ($migrations['list']) {
        $output .= "Ready<br>";
      }
      else {
        $output .= "Busy<br>";
      }
      $exec = 'Drupal\commerce_import\Form\ExecMigrations';
      $form = \Drupal::formBuilder()->getForm($exec, $migrations['list']);
    }

    return [
      'output' => ['#markup' => $output],
      'form' => $form,
      'migr-table' => [
        '#type' => 'table',
        '#header' => $this->buildHeader(),
        '#rows' => $migrations['list'],
      ],
    ];
  }

  /**
   * Header.
   */
  public function export() {
    $updater = \Drupal::service('commerce_import.update');
    $head = $updater->map;
    return [
      'output' => ['#markup' => '$output'],
      'table' => [
        '#type' => 'table',
        '#header' => $updater->map,
        '#rows' => $updater->export(),
      ],
    ];
  }

  /**
   * Header.
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Migration'),
      'status' => $this->t('Status'),
      'total' => $this->t('Total'),
      'imported' => $this->t('Imported'),
      'unprocessed' => $this->t('Unprocessed'),
      'messages' => $this->t('Messages'),
      'last' => $this->t('Last'),
    ];
    return $header;
  }

}
