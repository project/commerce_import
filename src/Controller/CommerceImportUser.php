<?php

namespace Drupal\commerce_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Commerce Import routes.
 */
class CommerceImportUser extends ControllerBase {

  /**
   * Builds the response.
   */
  public function page() {
    $formname = 'Drupal\commerce_import\Form\UserForm';

    $result = [
      'text' => [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => 'Импорт файла позволяет загрузить товары на сайт. Если в файле нет id уже имеющегося на сайте товара, то изменения его не затронут.',
      ],
      'form' => $this->formBuilder()->getForm($formname, []),
    ];

    return $result;
  }

}
