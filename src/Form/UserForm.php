<?php

namespace Drupal\commerce_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class UserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_import_user';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_import.settings');
    $directory = 'public://commerce-import/';
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $form['update_products'] = [
      '#type' => 'details',
      '#title' => $this->t('Update Products'),
      '#open' => TRUE,
      'file' => [
        '#title' => $this->t('Source file'),
        '#type' => 'managed_file',
        '#upload_location' => $directory,
        '#upload_validators' => [
          'file_validate_extensions' => ['xml csv json'],
          'file_validate_size' => [10485760],
        ],
        '#description' => $this->t('xml, csv or json'),
        // '#default_value' => $this->getFile(),
      ],
      'actions' => [
        'upload' => [
          '#type' => 'submit',
          '#value' => $this->t('Upload'),
          '#button_type' => 'primary',
          '#submit' => ['::submitFormUpload'],
        ],
      ],
    ];

    $form['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Upload and Run'),
        '#button_type' => 'primary',
      ],
      'run' => [
        '#type' => 'submit',
        '#value' => $this->t('Run'),
        '#button_type' => 'primary',
        '#ajax'   => [
          'callback' => '::ajaxExec',
          'effect'   => 'fade',
          'progress' => ['type' => 'throbber', 'message' => ""],
        ],
      ],
      '#suffix' => '<div id="file-result"></div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    if (isset($file[0]) && $file = File::load($file[0])) {
      $exec = \Drupal::service('commerce_import.exec')->exec(TRUE, FALSE);
      \Drupal::messenger()->addWarning("Запустили импорт");
    }
    else {
      \Drupal::messenger()->addError('File not found');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormUpload(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    if (isset($file[0]) && $file = File::load($file[0])) {

    }
    else {
      \Drupal::messenger()->addError('File not found');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxExec(array &$form, FormStateInterface $form_state) {
    $file = $this->getFile();
    $response = new AjaxResponse();
    if ($file) {
      $exec = \Drupal::service('commerce_import.exec')->exec(TRUE, FALSE);
      $response->addCommand(new HtmlCommand("#file-result", "<pre>$exec</pre>"));
    }
    else {
      $response->addCommand(new HtmlCommand("#file-result", $this->t('File not found')));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  private function getFile() {
    $file = FALSE;
    $files = $this->query();
    if (!empty($files)) {
      $file = array_shift($files);
    }
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  private function query() {
    $files = [];
    $storage = \Drupal::entityTypeManager()->getStorage('file');
    $query = $storage->getQuery()
      ->condition('status', 0)
      ->condition('uri', '%commerce-import/%', 'LIKE')
      ->sort('created', 'DESC')
      ->accessCheck(TRUE)
      ->range(0, 1);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($storage->loadMultiple($ids) as $id => $entity) {
        $files[$id] = $entity;
      }
    }
    return $files;
  }

}
