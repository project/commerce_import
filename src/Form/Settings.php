<?php

namespace Drupal\commerce_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Symfony\Component\Yaml\Yaml;
use Drupal\Core\Cache\Cache;

/**
 * Implements the form controller.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * SettingsFormWarning constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundle
   *   Entity Manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $moduleHandler,
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfo $entityTypeBundle
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundle = $entityTypeBundle;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_import_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_import.settings',
      'migrate_plus.migration.commerce_taxonomy_catalog',
      'migrate_plus.migration.commerce_product_variation',
      'migrate_plus.migration.commerce_product',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_import.settings');
    $catalog = $this->config('migrate_plus.migration.commerce_taxonomy_catalog');
    $product = $this->config('migrate_plus.migration.commerce_product');
    $variation = $this->config('migrate_plus.migration.commerce_product_variation');
    $commerce_import = $this->getMigrationPlugins('plugin.manager.commerce_import');
    $source_plugins = $this->getMigrationPlugins('plugin.manager.migrate.source');
    $form['source'] = [
      '#type' => 'details',
      '#title' => $this->t('Source'),
      '#open' => TRUE,
      'source_plugin' => [
        '#type' => 'select',
        '#title' => $this->t('Source Plugin'),
        '#required' => TRUE,
        '#options' => $commerce_import,
        '#default_value' => $config->get('source_plugin'),
      ],
    ];
    $form['mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Mapping'),
      '#open' => TRUE,
      'vocabulary' => [
        '#title' => $this->t('Catalog Vocabulary'),
        '#type' => 'select',
        '#options' => $this->getBundles('taxonomy_term', 'taxonomy'),
        '#default_value' => $config->get('vocabulary'),
      ],
      'product' => [
        '#title' => $this->t('Product Destination'),
        '#type' => 'select',
        '#options' => $this->getBundles('commerce_product'),
        '#default_value' => $config->get('product'),
      ],
      'variation' => [
        '#title' => $this->t('Product Variation Destination'),
        '#type' => 'select',
        '#options' => $this->getBundles('commerce_product_variation'),
        '#default_value' => $config->get('variation'),
      ],
    ];
    if ($catalog->get('process')) {
      $form['migrations-catalog'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Catalog'),
        '#open' => TRUE,
        'catalog-source' => [
          '#type' => 'select',
          '#title' => 'Catalog Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $catalog->get('source')['plugin'],
        ],
        'catalog-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($catalog->get('process'), 4),
        ],
      ];
    }
    if ($variation->get('process')) {
      $form['migrations-variations'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Product Variations'),
        '#open' => TRUE,
        'variations-source' => [
          '#type' => 'select',
          '#title' => 'Product Variations Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $variation->get('source')['plugin'],
        ],
        'variations-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($variation->get('process'), 4),
        ],
      ];
    }
    if ($product->get('process')) {
      $form['migrations-product'] = [
        '#type' => 'details',
        '#title' => $this->t('Migrations Product'),
        '#open' => TRUE,
        'product-source' => [
          '#type' => 'select',
          '#title' => 'Product Source Plugin',
          '#options' => $source_plugins,
          '#default_value' => $product->get('source')['plugin'],
        ],
        'product-process' => [
          '#title' => 'process',
          '#type' => 'textarea',
          '#attributes' => ['data-yaml-editor' => 'true'],
          '#default_value' => Yaml::dump($product->get('process'), 4),
        ],
      ];
    }
    $directory = 'public://commerce-import/';
    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $source = '/admin/commerce/import/export.csv';
    $form['update_products'] = [
      '#type' => 'details',
      '#title' => $this->t('Update Products'),
      '#open' => TRUE,
      'file' => [
        '#title' => $this->t('Source file'),
        '#type' => 'managed_file',
        '#upload_location' => $directory,
        '#upload_validators' => [
          'file_validate_extensions' => ['xml csv json'],
          'file_validate_size' => [10485760],
        ],
        '#description' => $this->t('csv, xml or json'),
      ],
      'actions' => [
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Update products'),
          '#button_type' => 'primary',
          '#submit' => ['::submitUpdate'],
        ],
      ],
    ];
    $drush_descr = $this->t("Leave blank to use default /usr/local/bin/drush");
    $form['drush_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Drush'),
      'drush' => [
        '#title' => $this->t('Drush'),
        '#type' => 'textfield',
        '#default_value' => $config->get('drush'),
        '#description' => $drush_descr,
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getMigrationPlugins($plugin) {
    $manager = FALSE;
    $plugins = [];
    try {
      $manager = \Drupal::service($plugin);
    }
    catch (\Exception $e) {
      return FALSE;
    }
    if ($manager) {
      foreach ($manager->getDefinitions() as $key => $source) {
        if (is_array($source['provider'])) {
          $plugins[$key] = "$key ({$source['provider'][0]})";
        }
        else {
          $plugins[$key] = "$key ({$source['provider']})";
        }
      }
    }
    return $plugins;
  }

  /**
   * Product Bundles.
   */
  private function getBundles($enity_type, $module = 'commerce_product') {
    $options = [];
    if ($this->moduleHandler->moduleExists($module)) {
      $bundles = $this->entityTypeBundle->getBundleInfo($enity_type);
      foreach ($bundles as $key => $value) {
        $options[$key] = $value['label'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('commerce_import.settings');
    $config
      ->set('source_plugin', $form_state->getValue('source_plugin'))
      ->set('product', $form_state->getValue('product'))
      ->set('variation', $form_state->getValue('variation'))
      ->set('vocabulary', $form_state->getValue('vocabulary'))
      ->set('drush', $form_state->getValue('drush'))
      ->save();
    $catalog = $this->config('migrate_plus.migration.commerce_taxonomy_catalog');
    if ($catalog->get('process')) {
      $catalog
        ->set('source', ['plugin' => $form_state->getValue('catalog-source')])
        ->set('process', Yaml::parse($form_state->getValue('catalog-process')))
        ->save();
    }
    $product = $this->config('migrate_plus.migration.commerce_product');
    if ($product->get('process')) {
      $product
        ->set('source', ['plugin' => $form_state->getValue('product-source')])
        ->set('process', Yaml::parse($form_state->getValue('product-process')))
        ->save();
    }
    $variation = $this->config('migrate_plus.migration.commerce_product_variation');
    if ($variation->get('process')) {
      $variation
        ->set('source', ['plugin' => $form_state->getValue('variations-source')])
        ->set('process', Yaml::parse($form_state->getValue('variations-process')))
        ->save();
    }
    $module_handler = \Drupal::moduleHandler();
    $module_handler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $service_id => $cache_backend) {
      $cache_backend
        ->deleteAll();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitUpdate(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    if (isset($file[0]) && $file = File::load($file[0])) {
      $path = $file->getFileUri();
      \Drupal::service('commerce_import.update')->run($path);
    }
    else {
      \Drupal::messenger()->addError('File not found');
    }
  }

}
