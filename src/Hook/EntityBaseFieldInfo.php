<?php

namespace Drupal\commerce_import\Hook;

/**
 * @file
 * Contains \Drupal\commerce_import\Hook\EntityBaseFieldInfo.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_entity_base_field_info().
 */
class EntityBaseFieldInfo {

  /**
   * Hook.
   */
  public static function hook(EntityTypeInterface $entity_type) {
    if ($entity_type->id() == 'commerce_product_variation') {
      $fields['product_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Product Key'))
        ->setDescription(t('Product Key for commerce_import search.'));
      return $fields;
    }
  }

}
