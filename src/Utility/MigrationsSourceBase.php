<?php

namespace Drupal\commerce_import\Utility;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for Plugins.
 */
class MigrationsSourceBase extends SourcePluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->plugin_id = $plugin_id;
    $this->trackChanges = TRUE;
    $this->setupDefaultProps();
    // Commerce Import.
    $this->cfg = \Drupal::config('commerce_import.settings');
    $plugin = \Drupal::service('plugin.manager.commerce_import');
    $source = $this->cfg->get('source_plugin');
    if ($source) {
      $this->src = $plugin->createInstance($source);
    }
    // SourcePlugin Settings.
    $this->config = self::accessProtected($migration, 'pluginDefinition')['process'];
    // Get Rows on Construct.
    if ($this->fetch) {
      $rows = $this->getRows();
      $this->rows = $rows;
    }
    // Debug.
    if ($this->uipage && $this->debug && \Drupal::moduleHandler()->moduleExists('devel')) {
      dsm($plugin_id . ": ProcessMapping & Rows");
      dsm($this->config);
      dsm($this->rows);
    }
  }

  /**
   * Get Rows.
   */
  public function getRows() {
    $rows = [];
    $this->rows = $rows;
    return $rows;
  }

  /**
   * Default Props.
   */
  private function setupDefaultProps() {
    $this->rows = [];
    // Fetch.
    if (!property_exists($this, 'fetch')) {
      $this->fetch = FALSE;
    }
    // Debug.
    if (!property_exists($this, 'debug')) {
      $this->debug = FALSE;
    }
    // UiPage.
    $this->uipage = FALSE;
    if (\Drupal::routeMatch()->getRouteName() == "entity.migration.list") {
      $this->uipage = TRUE;
    }
  }

  /**
   * Access Protected Obj Property.
   */
  public static function accessProtected($obj, $prop) {
    $reflection = new \ReflectionClass($obj);
    $property = $reflection->getProperty($prop);
    $property->setAccessible(TRUE);
    return $property->getValue($obj);
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    $rows = $this->getRows();
    return new \ArrayIterator($rows);
  }

  /**
   * Allows class to decide how it will react when it is treated like a string.
   */
  public function __toString() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getIDs() {
    return [
      'id' => [
        'type' => 'string',
        'alias' => 'id',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'id' => $this->t('Id-Key'),
    ];
    foreach ($this->config as $key => $config) {
      if (is_string($config)) {
        $fields[$key] = $config;
      }
      else {
        $fields[$key] = "[] data";
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    if ($this->fetch) {
      if ($this->plugin_id == 'commerce_tx_catalog') {
        $rows = $this->getRows();
        return count($rows);
      }
      if ($this->plugin_id == 'commerce_product') {
        $rows = $this->getRows();
        return count($rows);
      }
      if ($this->plugin_id == 'commerce_product_variation') {
        $rows = $this->getRows();
        return count($rows);
      }
      if ($this->plugin_id == 'commerce_image') {
        $rows = $this->getRows();
        return count($rows);
      }
      if ($this->plugin_id == 'commerce_product_attributes') {
        $rows = $this->getRows();
        return count($rows);
      }
    }
    else {
      return -1;
    }
    return count($this->rows);
  }

}
