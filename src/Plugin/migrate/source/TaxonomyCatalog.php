<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_tx_catalog"
 * )
 */
class TaxonomyCatalog extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $rows = [];
    $source = $this->src->catalog();
    $vocabulary = $this->cfg->get('vocabulary');
    $this->images = $this->queryMap('migrate_map_commerce_image');
    if ($source) {
      $k = 0;
      foreach ($source as $key => $category) {
        if ($k++ < 700 || !$this->uipage) {
          $row = [
            'vid' => $vocabulary,
            'id' => $category['id'],
            'status' => 1,
            'name' => $category['name'],
            'parent' => $category['parent'],
            'weight' => $category['weight'],
            'path' => empty($category['path']) ? FALSE : [
              'pathauto' => 0,
              'alias' => $category['path'],
            ],
          ];
          if (!empty($category['img']['uri'])) {
            $this->setFieldImage($row, $category['img']['uri']);
          }
          $rows[$key] = $row;
        }
      }
    }
    return $rows;
  }

  /**
   * Set Images.
   */
  private function setFieldImage(&$prod, $sourceid1) {
    if (isset($this->images[$sourceid1])) {
      $prod['field_catalog_image'] = [
        'target_id' => $this->images[$sourceid1],
      ];
    }
  }

  /**
   * Query MAP.
   */
  private function queryMap($table) {
    $data = [];
    $db = \Drupal::database();
    if (!$db->schema()->tableExists($table)) {
      return [];
    }
    $query = $db->select($table, 'map')->fields('map', [
      'sourceid1',
      'destid1',
    ]);
    $res = $query->execute();
    if ($res) {
      foreach ($res as $key => $row) {
        $data[$row->sourceid1] = $row->destid1;
      }
    }
    return $data;
  }

}
