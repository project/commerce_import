<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_image"
 * )
 */
class CommerceImage extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $rows = [];
    $source = $this->src->image();
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($source) {
      foreach ($source as $id => $image) {
        if ($k++ < 100 || !$this->uipage) {
          $id = $image['uri'];
          if (!empty($image['uri'])) {
            $row = [
              'id' => $id,
              'uid' => 1,
              'status' => TRUE,
              'langcode' => $lang,
              'uri' => $image['uri'],
              'filename' => $image['filename'],
            ];
          }
          $rows[$id] = $row;
        }
      }
    }
    $rows = array_values($rows);
    return $rows;
  }

}
