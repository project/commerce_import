<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_product"
 * )
 */
class CommerceProduct extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $this->fetch = TRUE;
    $this->debug = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $users = array_keys([
      1 => 'admin',
    ]);
    $uid = array_shift($users);
    $rows = [];
    $source = $this->src->product();
    $type = $this->cfg->get('product');
    if ($source) {
      // Some Magick.
      $this->store = $this->getStore();
      $this->catalog = $this->queryMap('migrate_map_commerce_taxonomy_catalog');
      // $this->variations = $this->queryMap('migrate_map_commerce_product_variation');
      $this->variations = $this->queryVariationsProducts();
      $this->images = $this->queryMap('migrate_map_commerce_image');
      $this->paragraphs = $this->queryMap('migrate_map_commerce_paragraphs', TRUE);
      foreach ($source as $product) {
        if ($k++ < 300  || !$this->uipage) {
          $id = $product['id'];
          $row = [
            'id' => $id,
            'uid' => $uid,
            'type' => $type,
            'stores' => $this->store,
            'status' => !empty($product['status']) ? $product['status'] : TRUE,
            'title' => trim($product['title']),
            'body' => empty($product['body']) ? FALSE : [
              'value' => $product['body'],
              'format' => 'basic_html',
            ],
            'path' => empty($product['path']) ? FALSE : [
              'pathauto' => 0,
              'alias' => $product['path'],
            ],
            'catalog' => [],
            'variations' => [],
            'field_short' => !empty($product['field_short']) ? $product['field_short'] : FALSE,
          ];
          $this->setCatalog($row, $product['catalog']);
          $this->setVariations($row, $id);
          if (!empty($product['img']['uri'])) {
            $this->setFieldImage($row, $product['img']['uri']);
          }
          // $this->setFieldParagraph($prod, $id);
          $rows[$id] = $row;
        }
      }
    }
    return $rows;
  }

  /**
   * Set Variations.
   */
  private function setCatalog(&$prod, $id) {
    if (isset($this->catalog[$id])) {
      $prod['field_catalog'] = [
        'target_id' => $this->catalog[$id],
      ];
    }
  }

  /**
   * Set Variations.
   */
  private function setVariations(&$prod, $id) {
    if (!empty($this->variations[$id])) {
      foreach ($this->variations[$id] as $variation) {
        $prod['variations'][] = [
          'target_id' => $variation,
        ];
      }
    }
  }

  /**
   * Set Images.
   */
  private function setFieldImage(&$prod, $sourceid1) {
    if (isset($this->images[$sourceid1])) {
      $prod['field_image'] = [
        'target_id' => $this->images[$sourceid1],
      ];
    }
  }

  /**
   * Set Paragraphs.
   */
  private function setFieldParagraph(&$prod, $id) {
    if (isset($this->paragraphs[$id])) {
      foreach ($this->paragraphs[$id] as $paragraph) {
        $prod['field_paragraph'][] = [
          'target_id' => $paragraph,
          'target_revision_id' => $paragraph,
        ];
      }
    }
  }

  /**
   * Query MAP.
   */
  private function queryMap($table, $subs = FALSE) {
    $data = [];
    $db = \Drupal::database();
    if (!$db->schema()->tableExists($table)) {
      return [];
    }
    $query = $db->select($table, 'map')->fields('map', [
      'sourceid1',
      'destid1',
    ]);
    $res = $query->execute();
    if ($res) {
      foreach ($res as $key => $row) {
        if ($subs) {
          $k = strstr($row->sourceid1, "-", TRUE);
          $data[$k][$row->sourceid1] = $row->destid1;
        }
        else {
          $data[$row->sourceid1] = $row->destid1;
        }
      }
    }
    return $data;
  }

  /**
   * GetStore.
   */
  private function getStore() {
    $sid = FALSE;
    /** @var Drupal\commerce_order\Entity\Order $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_store');
    if ($store = $storage->loadDefault()) {
      $sid = $store->id();
    }
    else {
      $ids = $storage->getQuery()
        ->range(0, 1)
        ->accessCheck(TRUE)
        ->execute();
      if (!empty($ids)) {
        $sid = array_shift($ids);
        $store = $storage->load($sid);
        $storage->markAsDefault($store);
      }
    }
    return $sid;
  }

  /**
   * Query MAP.
   */
  private function queryVariationsProducts() {
    $variations = [];
    $table = 'commerce_product_variation_field_data';
    $db = \Drupal::database();
    if (!$db->schema()->tableExists($table)) {
      return [];
    }
    $query = $db
      ->select($table, 'variations')
      ->fields('variations', [
        'variation_id',
        'product_key',
      ])
      ->isNotNull('product_key');
    $res = $query->execute();

    if ($res) {
      foreach ($res as $key => $row) {
        $product_key = $row->product_key;
        $variations[$product_key][] = $row->variation_id;
      }
    }
    return $variations;
  }

}
