<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_tx_terms"
 * )
 */
class TaxonomyTerms extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows($rows = []) {
    $k = 0;
    $source = $this->src->term();
    $volcabs = [
      '1' => 'tags',
    ];
    if ($source) {
      foreach ($source as $key => $row) {
        if ($k++ < 700 || !$this->uipage) {
          $id = $row['tid'];
          $vid = $row['vid'];
          $vocabulary = isset($volcabs[$vid]) ? $volcabs[$vid] : 'default';
          $rows[$id] = [
            'id' => $id,
            'vid' => $vocabulary,
            'name' => $row['name'],
            'status' => 1,
            'weight' => $row['weight'],
            'path' => empty($row['path']) ? FALSE : [
              'pathauto' => 0,
              'alias' => "/{$row['path']}",
            ],
          ];
          if (!empty($row['parent'])) {
            $rows[$id]['parent'] = $row['parent'];
          }
        }
      }
    }
    $this->debug = FALSE;
    return $rows;
  }

}
