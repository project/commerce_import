<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_paragraphs"
 * )
 */
class CommerceParagraphs extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function constructOn() {
    // $this->getRows();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $rows = [];
    $source = $this->src->paragraphs();
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($source) {
      foreach ($source as $id => $product) {
        foreach ($product as $key => $paragraphs) {
          if ($k++ < 40 || !$this->uipage) {
            $paragraphs['id'] = $key;
            $paragraphs['status'] = TRUE;
            $paragraphs['langcode'] = $lang;
            $paragraphs['type'] = 'product_param';
            $rows[$key] = $paragraphs;
          }
        }
      }
    }
    print "ROWS COUNT: " . count($rows) . "\n";
    return $rows;
  }

}
