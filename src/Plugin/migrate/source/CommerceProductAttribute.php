<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for attributes.
 *
 * @MigrateSource(
 *   id = "commerce_product_attributes"
 * )
 */
class CommerceProductAttribute extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $users = array_keys([
      1 => 'admin',
    ]);
    $uid = array_shift($users);
    $rows = [];
    $source = FALSE;
    // $source = $this->src->variation();
    // $type = $this->cfg->get('variation');
    if ($source) {
      foreach ($source as $attribute) {
        if ($k++ < 300 || !$this->uipage) {
          $id = $attribute['id'];
          $rows[$id] = [
            'id' => $id,
            'attribute' => $attribute['attribute'],
            'name' => $attribute['name'],
            'weight' => $attribute['weight'],
          ];
        }
      }
    }
    return $rows;
  }

}
