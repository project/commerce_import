<?php

namespace Drupal\commerce_import\Plugin\migrate\source;

use Drupal\commerce_import\Utility\MigrationsSourceBase;

/**
 * Source for CSV.
 *
 * @MigrateSource(
 *   id = "commerce_product_variation"
 * )
 */
class CommerceProductVariation extends MigrationsSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getRows() {
    $k = 0;
    $users = array_keys([
      1 => 'admin',
    ]);
    $uid = array_shift($users);
    $rows = [];
    $source = $this->src->variation();
    $type = $this->cfg->get('variation');
    if ($source) {
      foreach ($source as $variation) {
        if ($k++ < 300 || !$this->uipage) {
          $id = $variation['id'];
          $rows[$id] = [
            'id' => $id,
            'uid' => $uid,
            'type' => $type,
            'title' => trim($variation['title']),
            'sku' => !empty($variation['sku']) ? $variation['sku'] : FALSE,
            'price' => [
              'number' => $variation['price'],
              'currency_code' => 'RUB',
            ],
            'list_price' => empty($variation['list_price']) ? FALSE : [
              'number' => $variation['list_price'],
              'currency_code' => 'RUB',
            ],
            'field_stock' => !empty($variation['field_stock']) ? $variation['field_stock'] : FALSE,
            'product_key' => !empty($variation['product_key']) ? $variation['product_key'] : FALSE,
            'field_oldprice' => !empty($variation['field_oldprice']) ? $variation['field_oldprice'] : FALSE,
          ];
        }
      }
    }
    return $rows;
  }

}
