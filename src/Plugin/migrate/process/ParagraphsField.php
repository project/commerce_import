<?php

namespace Drupal\commerce_import\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Converts Migration Lookup results into target_id and target_revision_id.
 *
 * Use this after using migration_lookup for entity reference and entity
 * reference revisions migrations. This solves issues with inconsistencies
 * between paragraph and node lookups.
 *
 * Example:
 *
 * @code
 * process:
 *   field_paragraph:
 *     -
 *       plugin: skip_on_empty
 *       method: process
 *       source: field_paragraph
 *     -
 *       plugin: paragraphs_field
 *       data: field_paragraph
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "paragraphs_field",
 *   handle_multiples = TRUE
 * )
 */
class ParagraphsField extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->migration = $migration;
    $this->storage = \Drupal::service('entity_type.manager')->getStorage('paragraph');
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // $id = $row->get('id');
    $result = [];
    foreach ($value as $param => $val) {
      if (is_array($val)) {
        $paragraph = reset($this->storage->loadByProperties($val));
        if (!is_object($paragraph)) {
          $paragraph = $this->storage->create([
            'type' => 'product_param',
            'field_product_param_param' => $val['field_product_param_param'],
            'field_product_param_value' => $val['field_product_param_value'],
          ]);
          $paragraph->save();
        }
        $result[] = [
          'target_id' => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ];
      }
    }
    return $result;
  }

}
