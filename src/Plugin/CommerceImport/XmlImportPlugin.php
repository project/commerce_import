<?php

namespace Drupal\commerce_import\Plugin\CommerceImport;

use Drupal\commerce_import\PluginManager\ImportPluginBase;
use Drupal\commerce_import\PluginManager\ImportPluginInterface;

/**
 * Provides a 'Xml' Template.
 *
 * @CommerceImportAnnotation(
 *   id = "xml",
 *   title = @Translation("Xml"),
 * )
 */
class XmlImportPlugin extends ImportPluginBase implements ImportPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function init() {
    $this->xml = [];
    $file = $this->getFile();
    if ($file) {
      $file = $this->getFile();
      $uri = $file->getFileUri();
      $path = \Drupal::service('file_system')->realpath($uri);
      $xmlstring = file_get_contents($path);
      $xmlobj = simplexml_load_string($xmlstring);
      $json = json_encode($xmlobj, JSON_FORCE_OBJECT);
    }

    $this->xml = json_decode($json, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function catalog() {
    $weight = 1;
    $root = 'S_21885';
    $catalog = [];
    if (!empty($this->xml['productgroups']['productgroup'])) {
      $categories = $this->xml['productgroups']['productgroup'];
      $weight = 0;
      foreach ($categories as $category) {
        $weight++;
        if (!empty($category['UID']) && $category['UID'] != $root) {
          $id = $category['UID'];
          $parent = FALSE;
          if ($category['parentUID'] != $root) {
            $parent = $category['parentUID'];
          }
          $catalog[$id] = [
            'id' => $id,
            'name' => $category['name'],
            'parent' => $parent,
            'weight' => $weight,
          ];
        }
      }
    }
    return $catalog;
  }

  /**
   * {@inheritdoc}
   */
  public function product() {
    $products = [];
    if (!empty($this->xml['products']['product'])) {
      foreach ($this->xml['products']['product'] as $product) {
        if (!empty($product['code1c'])) {
          $id = $product['code1c'];
          $enable = $product['enable'];
          $barcodes = $product['barcodes'];
          $products[$id] = [
            'id' => $id,
            'type' => 'product',
            'title' => $product['name'],
            'body' => $product['description'],
            'catalog' => $product['parentUID'],
            'field_short' => $product['brief_description'],
          ];
        }
      }
    }
    return $products;
  }

  /**
   * {@inheritdoc}
   */
  public function variation() : array {
    $variations = [];
    if (!empty($this->xml['products'])) {
      $products = $this->xml['products']['product'];
      foreach ($products as $product) {
        if (!empty($product['code1c'])) {
          $id = $product['code1c'];
          $variations[$id] = [
            'id' => $id,
            'type' => 'variation',
            'title' => $product['name'],
            'sku' => '',
            'price' => $product['price'],
            'list_price' => $product['sale_price'],
            'stock' => $product['in_stock'],
            'product_id' => $id,
            'product_key' => $id,
          ];
        }
      }
    }
    return $variations;
  }

  /**
   * {@inheritdoc}
   */
  public function image() {
    $images = [];
    return $images;
  }

  /**
   * {@inheritdoc}
   */
  public function paragraphs() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function term() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  private function getFile() {
    $file = FALSE;
    $files = $this->query();
    if (!empty($files)) {
      $file = array_shift($files);
    }
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  private function query() {
    $files = [];
    $storage = \Drupal::entityTypeManager()->getStorage('file');
    $query = $storage->getQuery()
      ->condition('status', 0)
      ->condition('uri', '%commerce-import/%', 'LIKE')
      ->sort('created', 'DESC')
      ->accessCheck(TRUE)
      ->range(0, 1);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($storage->loadMultiple($ids) as $id => $entity) {
        $files[$id] = $entity;
      }
    }
    return $files;
  }

}
