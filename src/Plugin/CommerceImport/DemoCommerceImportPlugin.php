<?php

namespace Drupal\commerce_import\Plugin\CommerceImport;

use Drupal\commerce_import\PluginManager\ImportPluginBase;
use Drupal\commerce_import\PluginManager\ImportPluginInterface;
use Drupal\Core\File\FileSystemInterface;

/**
 * Provides a 'CSV' Template.
 *
 * @CommerceImportAnnotation(
 *   id = "demo",
 *   title = @Translation("Demo Import"),
 * )
 */
class DemoCommerceImportPlugin extends ImportPluginBase implements ImportPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function init() {
    $src = 'https://raw.githubusercontent.com/synapse-studio/helper/master/content/slide';
    $this->data = [
      'catalog' => [
        'c1' => ['name' => 'Осень', 'img' => "$src/autumn.jpg"],
        'c2' => ['name' => 'Зима', 'img' => "$src/winter.jpg"],
        'c3' => ['name' => 'Горы', 'img' => "$src/mountain.jpg"],
        'c4' => [
          'name' => 'Море',
          'parent' => 'c2',
          'img' => "$src/sea.jpg",
        ],
        'c5' => [
          'name' => 'Зима2',
          'parent' => 'c2',
          'img' => "$src/winter.jpg",
        ],
        'c6' => [
          'name' => 'Горы2',
          'parent' => 'c2',
          'img' => "$src/mountain.jpg",
        ],
        'c7' => [
          'name' => 'Осень2',
          'parent' => 'c4',
          'img' => "$src/autumn.jpg",
        ],
        'c8' => [
          'name' => 'Море2',
          'parent' => 'c4',
          'img' => "$src/sea.jpg",
        ],
      ],
      'products' => [
        'SKU1' => [
          'title' => 'I1: Moscow',
          'catalog' => 'c1',
          'sku' => 'SKU1',
          'price' => 100,
          'img' => "$src/moscow.jpg",
        ],
        'SKU2' => [
          'title' => 'I2: Spb',
          'catalog' => 'c1',
          'sku' => 'SKU2',
          'price' => 150,
          'img' => "$src/spb.jpg",
        ],
        'SKU3' => [
          'title' => 'I3: Tundra',
          'catalog' => 'c7',
          'sku' => 'SKU3',
          'price' => 200,
          'img' => "$src/tundra.jpg",
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function catalog() {
    $weight = 1;
    $catalog = [];
    foreach ($this->data['catalog'] as $id => $category) {
      $catalog[$id] = [
        'id' => $id,
        'name' => $category['name'],
        'img' => $this->fileUrlToUri($category['img']),
        'parent' => !empty($category['parent']) ? $category['parent'] : FALSE,
        'weight' => $weight++,
      ];
    }
    return $catalog;
  }

  /**
   * {@inheritdoc}
   */
  public function product() {
    $products = [];
    foreach ($this->data['products'] as $id => $product) {
      $products[$id] = [
        'id' => $id,
        'title' => $product['title'],
        'catalog' => $product['catalog'],
        'img' => $this->fileUrlToUri($product['img']),
      ];
    }
    return $products;
  }

  /**
   * {@inheritdoc}
   */
  public function variation() {
    $variations = [];
    foreach ($this->data['products'] as $id => $variation) {
      $variations[$id] = [
        'id' => $id,
        'sku' => $variation['sku'],
        'title' => $variation['title'],
        'price' => $variation['price'],
      ];
    }
    return $variations;
  }

  /**
   * {@inheritdoc}
   */
  public function paragraphs() {
    $paragraphs = [];
    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function image() {
    $images = [];
    foreach ($this->product() as $product) {
      if (!empty($product['img'])) {
        $images[] = [
          'entity' => 'product',
          'id' => $product['id'],
          'img' => $product['img'],
          'download' => TRUE,
        ];
      }
    }
    foreach ($this->catalog() as $category) {
      if (!empty($category['img'])) {
        $images[] = [
          'entity' => 'taxonomy_term',
          'id' => $category['id'],
          'img' => $category['img'],
          'download' => TRUE,
        ];
      }
    }
    // Download images.
    foreach ($images as $key => $image) {
      if (!empty($image['img']['uri'])) {
        $images[$key]['uri'] = $image['img']['uri'];
        $images[$key]['filename'] = $image['img']['filename'];
      }
      if (!empty($image['download'])) {
        $src = $image['img']['src'];
        $download[$src] = $image['img']['uri'];
      }
    }
    if (FALSE && !empty($download)) {
      $dir = 'public://import/demo';
      $this->downloadImages($dir, $download);
    }
    return $images;
  }

  /**
   * {@inheritdoc}
   */
  private function fileUrlToUri(string $src) : array {
    $dir = 'public://import/demo';
    $src_args = explode('/', $src);
    $filename = end($src_args);
    return [
      'src' => $src,
      'uri' => "$dir/$filename",
      'filename' => $filename,
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function downloadImages(string $dir, array $download) : array {
    $fs = \Drupal::service('file_system');
    $files = [];
    if ($fs->prepareDirectory($dir)) {
      foreach ($download as $src => $uri) {
        $file = file_get_contents($src);
        $uri = $fs->saveData($file, $uri, FileSystemInterface::EXISTS_REPLACE);
        $files[] = $uri;
      }
    }
    return $files;
  }

}
