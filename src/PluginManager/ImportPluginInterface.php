<?php

namespace Drupal\commerce_import\PluginManager;

/**
 * Defines the common interface for all commerce_import classes.
 *
 * @see \Drupal\commerce_import\PluginManager\CommerceImportManager
 * @see \Drupal\commerce_import\PluginManager\CommerceImportAnnotation
 * @see plugin_api
 */
interface ImportPluginInterface {

  /**
   * On __construct.
   */
  public function init();

  /**
   * Catalog.
   */
  public function catalog();

  /**
   * Product Variation.
   */
  public function variation();

  /**
   * Commerce Product.
   */
  public function product();

  /**
   * File image.
   */
  public function image();

  /**
   * Paragraphs.
   */
  public function paragraphs();

  /**
   * Terms.
   */
  public function term();

}
