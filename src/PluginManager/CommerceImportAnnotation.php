<?php

namespace Drupal\commerce_import\PluginManager;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an commerce_import template object.
 *
 * Plugin Namespace: Plugin\CommerceImport.
 *
 * @see \Drupal\commerce_import\PluginManager\CommerceImportManagers
 * @see \Drupal\commerce_import\PluginManager\ImportPluginInterface
 * @see plugin_api
 * @see hook_archiver_info_alter()
 *
 * @Annotation
 */
class CommerceImportAnnotation extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

}
