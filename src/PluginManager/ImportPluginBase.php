<?php

namespace Drupal\commerce_import\PluginManager;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides an Archiver plugin manager.
 *
 * @see Drupal\commerce_import\PluginManager\CommerceImportAnnotation
 * @see Drupal\commerce_import\PluginManager\ImportPluginInterface
 * @see plugin_api
 */
class ImportPluginBase extends PluginBase {

  /**
   * Construct.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->init();
  }

  /**
   * Create.
   */
  public function init() {

  }

  /**
   * Terms.
   */
  public function term() {
    return [];
  }

  /**
   * Images.
   */
  public function image() {
    return [];
  }

}
