<?php

namespace Drupal\commerce_import\PluginManager;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Archiver plugin manager.
 *
 * @see \Drupal\commerce_import\PluginManager\ImportPluginInterface
 * @see \Drupal\commerce_import\PluginManager\CommerceImportAnnotation
 * @see plugin_api
 */
class CommerceImportManager extends DefaultPluginManager {

  /**
   * Constructs a ArchiverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/CommerceImport',
      $namespaces,
      $module_handler,
      'Drupal\commerce_import\PluginManager\ImportPluginInterface',
      'Drupal\commerce_import\PluginManager\CommerceImportAnnotation'
    );
    $this->alterInfo('commerce_import_info');
    $this->setCacheBackend($cache_backend, 'commerce_import_info_plugins');
  }

  /**
   * VHosts - Plugable Options.
   */
  public function optionsList() {
    $options = [];
    foreach ($this->getDefinitions() as $key => $plugin) {
      $info = ($plugin['provider'] == 'commerce_import') ? "" : " / {$plugin['provider']}";
      $options[$key] = $plugin['title']->__toString() . "$info";
    }
    return $options;
  }

}
